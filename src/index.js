import React from "react";
import ReactDOM from "react-dom";
import LineChart from "./lib/CourseLib";

import "./styles.css";

function App() {
  const data = [];
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      <hr />
      <LineChart data={data} witdh={800} height={640} />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
