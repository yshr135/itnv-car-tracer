import React, { useEffect } from "react";
import d3 from "d3";

const drawLine = (x, y, r, color) => {
  // const data = [15, 20, 5];
  const circle = d3.selectAll("circle");
  circle
    .attr("cx", x)
    .attr("cy", y)
    .attr("r", r)
    .attr("fill", color);

  // const height = svg.attr("height");
  // const g = svg
  //   .selectAll("g")
  //   .append("g")
  //   .join("g");
  // g.attr("cx", 100)
  //   .attr("cy", 100)
  //   .attr("r", 20)
  //   .attr("fill", "pink");

  // const circle = svg.selectAll("circle").data(data);
  // circle
  //   .attr("cx", (_, i) => i * 50 + 50)
  //   .attr("cy", -100)
  //   .attr("r", d => d)
  //   .attr("fill", "pink")
  //   .transition()
  //   .duration((_, i) => (i + 1) * 2000)
  //   .ease(d3.easeBounce)
  //   .attr("cy", 100)
  //   .transition()
  //   .duration((_, i) => (i + 1) * 2000)
  //   .attr("cy", height + 100);
};

export default ({ x, y, r, color }) => {
  useEffect(() => {
    drawLine(x, y, r, color);
  });
  return <circle />;
};
