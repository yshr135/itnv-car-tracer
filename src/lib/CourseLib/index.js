import React, { useEffect } from "react";
import * as d3 from "d3";

const plotPoint = () => {
  const data = [{ x: 10, y: 10 }, { x: 20, y: 10 }, { x: 20, y: 5 }];
  const svg = d3.selectAll("svg");
  d3.map(data, d => {
    svg
      .append("circle")
      .attr("cx", d.x)
      .attr("cy", d.y)
      .attr("r", 5)
      .attr("fill", "black");
  });
};

export default ({ witdh, height }) => {
  useEffect(() => {
    plotPoint();
  });
  return <svg witdh={witdh} height={height} />;
};
